# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tchivert <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/29 15:27:03 by tchivert          #+#    #+#              #
#    Updated: 2019/05/21 16:19:00 by tchivert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = todo

SRC = srcs/todo.c srcs/libft/libft.a

INCLUDES = srcs
CC = clang
CFLAGS = -Werror -Wall -Wextra

all: $(NAME)

$(NAME): libft $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o $(NAME)

libft:
	make -C srcs/libft fclean
	make -C srcs/libft
	make -C srcs/libft clean

clean:
	/bin/rm -f $(OBJ)
	make -C srcs/libft clean

fclean: clean
	/bin/rm -f $(NAME)
	make -C srcs/libft fclean

re: fclean all
