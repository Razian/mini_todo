/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   todo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/16 14:49:26 by tchivert          #+#    #+#             */
/*   Updated: 2019/05/21 16:09:09 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "todo.h"
#include <stdio.h>

static void	t_help()
{
	ft_putstr("todo, version 0.1\n\n");
	ft_putstr("usage: todo [command] [option]\n\n");
	ft_putstr("  add \"entry\": add a new entry in your list\n");
	ft_putstr("  del x : remove the line x of your list\n");
	ft_putstr("  ls : show your list\n");
	ft_putstr("  help : show this menu\n");
}

static void t_ls(int fd)
{
	char	*line;
	int		i;

	i = 1;
	while (get_next_line(fd, &line))
	{
		ft_putnbr(i);
		ft_putstr(". ");
		ft_putstr(line);
		ft_putchar('\n');
		free(line);
		i++;
	}
}

static void	t_add(int fd, char *str)
{
	char	*line;
	char	*tmp = NULL;
	size_t		i;

	i = read(fd, tmp, 1);
	while (get_next_line(fd, &line))
	{
		ft_strdel(&line);
	}
	if (i != 0)
		ft_putchar_fd('\n', fd);
	ft_putstr_fd(str, fd);
}

static int	t_del(int fd, char *dir, char *nbr)
{
	int		i;
	int		fd2;
	char	*file;
	char	*tmp;
	char	*line;

	i = 1;
	if (!(file = ft_strnew(1)))
		return (-1);
	while (get_next_line(fd, &line))
	{
		if (i != ft_atoi(nbr))
		{
			tmp = file;
			file = ft_strjoin(file, line);
			ft_strdel(&tmp);
			tmp = file;
			file = ft_strjoin(file, "\n");
			ft_strdel(&tmp);
		}
		ft_strdel(&line);
		i++;
	}
	fd2 = open(ft_strjoin(dir, "replicat.txt"), O_CREAT|O_RDWR, 0644);
	ft_putstr_fd(file, fd2);
	close(fd);
	close(fd2);
	remove(ft_strjoin(dir, "todo.txt"));
	rename(ft_strjoin(dir, "replicat.txt"), ft_strjoin(dir, "todo.txt"));
	fd = open(ft_strjoin(dir, "todo.txt"), O_RDWR);
	return (fd);
}

int	main(int ac, char **av)
{
	int		fd;
	int		i;
	char	*home;
	char	*dir;

	home = getenv("HOME");
	dir = ft_strcat(home, "/mini_todo/");
	fd = open(ft_strjoin(dir, "todo.txt"), O_CREAT|O_RDWR, 0644);
	if (ac == 1)
	{
		ft_putstr("Please enter a command\n");
		ft_putstr("Type \"todo help\" to show help\n");
		return (0);
	}
	if (ft_strcmp(av[1], "ls") == 0)
	{
		t_ls(fd);
		return (0);
	}
	if (ft_strcmp(av[1], "help") == 0)
	{
		t_help();
		return (0);
	}
	if (ac == 2)
	{
		ft_putstr("Please enter an option\n");
		ft_putstr("Type \"todo help\" to show help\n");
		return (0);
	}
	i = 2;
	if (ft_strcmp(av[1], "add") == 0)
	{
		while (av[i])
		{
			t_add(fd, av[i]);
			i++;
		}
	}
	if (ft_strcmp(av[1], "del") == 0)
	{
		while (av[i])
		{
			fd = t_del(fd, dir, av[i]);
			i++;
		}
	}
	close(fd);
	return (0);
}
