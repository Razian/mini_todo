# mini_todo

Super simple cli todo-list inspired by todotxt, written in C to learn how to manage a file from my functions.
It's working but is incomplete and the code is pretty bad, I have to clean this up :)

I included my libft, it's my personnal C library written at school, and used in the project.

`todo` is the binary, and the code is in the `srcs/` folder.

### Install :
```
cd $HOME/
git clone https://gitlab.com/razian/mini_todo
alias todo="$HOME/mini_todo/todo"
```

And you can set the alias in your .bashrc/.zshrc : `echo "alias todo=\"$HOME/mini_todo/todo\"" >> .bashrc`

### Compile from sources :
```
make -C $HOME/mini_todo
```

### Usage :

```
usage: todo [command] [option]

  add "entry": add a new entry in your list
  del x : remove the line x of your list
  ls : show your list
  help : show this menu
```